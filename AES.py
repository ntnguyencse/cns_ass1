import os
from cryptography.fernet import Fernet
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.backends import *
import hashlib
from tkinter import *
from tkinter import ttk
from tkinter.filedialog import askopenfilename
from tkinter.filedialog import askdirectory
from Crypto.Cipher import DES3
window = Tk()
progress_bar=ttk.Progressbar(window,orient=HORIZONTAL,length=200)
progress_bar.config(mode='determinate')
#Progress bar variable
file_size=0
file_progress=0
#####
backend = default_backend()

#Text_log
#text_log=Text(window,width=90,height=10)
text_log=Text(window,width=95,height=15)
###########################################
def read_blocks(f, size=1024):
    global file_progress
    while True:
        data = f.read(size)
        file_progress=file_progress+1
        if not data:
            break
        yield data
##########################################
#AES Encryption Function
def AESencrypt(infile, outfile, key):
    iv = os.urandom(12)
    encryptor = Cipher(
            algorithms.AES(key),
            modes.GCM(iv),
            backend=backend,
            ).encryptor()
    with open(infile, 'rb') as in_file:
        with open(outfile, 'wb') as out_file:
            out_file.write(iv)
            for block in read_blocks(in_file):
                out_file.write(encryptor.update(block))
            out_file.write(encryptor.finalize())
            out_file.write(encryptor.tag)

#AES Decrytion function
def AESdecrypt(infile, outfile, key):
    with open(infile, 'rb') as in_file:
        with open(outfile, 'wb') as out_file:
            iv =in_file.read(12)
            if len(iv) != 12:
                raise Exception('IV of incorrect length was read from input.')
            decryptor = Cipher(
                algorithms.AES(key),
                modes.GCM(iv),
                backend=backend,
            ).decryptor()
            buff = b''
            for block in read_blocks(in_file):
                block = buff + block
                buff = block[-16:]
                block = block[:-16]
                out_file.write(decryptor.update(block))
            try:
                decryptor.finalize_with_tag(buff)
            except:
                print("Key incorrect")
                text_log.insert(INSERT,"Key incorrect\n")
            in_file.close()
            out_file.close()
#######################################################
## Camellia Encryption/Decryption
#def TripleDES_encrypt(in_file, out_file, key):
#    iv = os.urandom(8)
#    des3 = DES3.new(key, DES3.MODE_CFB, iv)
#    out_file.write(iv)
#    for block in read_blocks(in_file,1):
#        out_file.write(des3.encrypt(block))
 
#def TripleDES_decrypt(in_file, out_file, key):
#    iv = in_file.read(8)
#    if len(iv) != 8:
#        raise Exception('IV of incorrect length was read from input.')
#    des3 = DES3.new(key, DES3.MODE_CFB, iv)
#    for block in read_blocks(in_file,1):
#        out_file.write(des3.decrypt(block))
def TripleDES_encrypt(in_filename, out_filename, key):
    chunk_size=8192
    iv=os.urandom(8)
    des3 = DES3.new(key, DES3.MODE_CFB, iv)
    
    with open(in_filename, 'rb') as in_file:
        with open(out_filename, 'wb') as out_file:
            out_file.write(iv)
            while True:
                chunk = in_file.read(chunk_size)
                if len(chunk) == 0:
                    break
                out_file.write(des3.encrypt(chunk))
        in_file.close()
        out_file.close()
def TripleDES_decrypt(in_filename, out_filename, key):
    
    chunk_size=8192
    with open(in_filename, 'rb') as in_file:
        with open(out_filename, 'wb') as out_file:
            iv =in_file.read(8)
            if len(iv) != 8: 
                raise Exception('IV is incorrect length')
            des3 = DES3.new(key, DES3.MODE_CFB, iv)

            while True:
                chunk = in_file.read(chunk_size)
                if len(chunk) == 0:
                    break
                out_file.write(des3.decrypt(chunk))
        in_file.close()
        out_file.close()
#######################################################
#Hash SHA256 function
def hash_SHA256(file_input):

    sha256_hash = hashlib.sha256()

    # Read and update hash string value in blocks of 4K
    for byte_block in read_blocks(file_input,4096):
        sha256_hash.update(byte_block)
    return sha256_hash.hexdigest()
def hash_MD5(file_input):

    md5_hash = hashlib.md5()

    # Read and update hash string value in blocks of 4K
    for byte_block in read_blocks(file_input,4096):
        md5_hash.update(byte_block)
    return md5_hash.hexdigest()
def hash_key(key):

    return hashlib.sha256(key.encode()).digest()
def hash_key_128(key):
    return hashlib.md5(key.encode()).digest()