from AES import *
from pathlib import Path
#Global Variables
algorithm=0
hash_algorithm=0
filename=''
hash=b''
hash128=b''
dir_name=''
list_file=[]
text_log=Text(window,width=95,height=15)
####
def set_text(text):
    input_file_name.delete(0,END)
    input_file_name.insert(0,text)
def set_dir(text):
    input_dir_name.delete(0,END)
    input_dir_name.insert(0,text)
def getFileName():
    global filename
    global progress_bar
    global file_size
    filename=askopenfilename()
    set_text(filename)
    if(filename!="0"):
        print(filename)
        text_log.insert(INSERT,"Choosed "+ filename+ "\n")
        progress_bar['maximum']=file_size
    else:
        print("File not found")
def find_file(dir_name,list_file):
    for path in Path(dir_name).iterdir():
        if path.is_file():
            list_file.append(str(path))
        elif path.is_dir():
            find_file(path,list_file)
def getDirname():
    global dir_name
    global list_file
    dir_name=askdirectory()
    set_dir(dir_name)
    if(dir_name!="0"):
        print(dir_name)
        text_log.insert(INSERT,"Choosed "+ dir_name+ "\n")
    list_file.clear()
    find_file(dir_name,list_file)

def get_file():
    return input_file_name.get()
def encrypt_():
    global file_progress
    global algorithm
    file_progress=0

    file_in=get_file()
    file_out=file_in+".enc"
    
    
    if(algorithm!=2):
        file_out=file_out+".aes"
        AESencrypt(file_in,file_out,hash)
    else:
        file_out=file_out+".des3"
        TripleDES_encrypt(file_in,file_out,hash128)
    text_log.insert(INSERT,"File: "+file_in+"\n.....Encrypted\n")
def decrypt_():
    file_in=get_file()
    
    a=file_in.find('.')
    file_out=file_in[:-8-(len(file_in)-a-8)]+"_dec"+file_in[a:len(file_in)-8]
    if(algorithm!=2):
        AESdecrypt(file_in,file_out,hash)
    else:
        TripleDES_decrypt(file_in,file_out,hash128)
    text_log.insert(INSERT,"File: "+file_in+"\n.....Decrypted\n")
#Generate hash key
def Generate_key(text):
    input_key_entry.delete(0,END)
    input_key_entry.insert(0,text)
#Get key input
def callback():
    global hash
    global hash128
    hash = hash_key(input_key_entry.get())
    hash128=hash_key_128(input_key_entry.get())
    print(hash_key(input_key_entry.get()))
    #print(hash)
    Generate_key(input_key_entry.get())
    text_log.insert(INSERT,"Key generated\n")
def find_aes(list_file,list_file_aes):
    
    for file_name in list_file:
        if (file_name.find(".enc.aes") > -1 ):
            list_file_aes.append(file_name)
            print(str(file_name)+"\n")
    
def find_des3(list_file,list_file_des3):
    
    for file_name in list_file:
        if (file_name.find(".enc.des3") >-1 ):
            list_file_des3.append(file_name)
            print(str(file_name)+"\n")
    
def folder_encrypt():
    global list_file
    global algorithm
    global hash
    global hash128
    
    for i in range(len(list_file)):
        path=list_file[i]
        file_in=path
    
        a=file_in.find('.')
        file_out=file_in+".enc"
        if(algorithm==1):
            file_out=file_out+".aes"
            AESencrypt(file_in,file_out,hash)
        else:
            file_out=file_out+".des3"
            TripleDES_encrypt(file_in,file_out,hash128)
        text_log.insert(INSERT,"File: "+file_in+"\nEncrypted\n")
    list_file.clear()
def folder_decrypt():
    global list_file
    global algorithm
    global hash
    global hash128
    find_file(dir_name,list_file)
    #print(list_file)
    print("\n")
    
    print("\n")
    if (algorithm !=2):
        list_aes=[]
        print(list_file)
        find_aes(list_file,list_aes)
        print(list_aes)
        print("\n")
        for i in list_aes:
            file_in=i
            a=file_in.find('.')
            file_out=file_in[:-8-(len(file_in)-a-8)]+"_dec"+file_in[a:len(file_in)-8]
            AESdecrypt(file_in,file_out,hash)
            text_log.insert(INSERT,"File: "+file_in+"\nDecrypted\n")
    else: 
        list_des3=[]
        find_des3(list_file,list_des3)
        print("\n")
        print(list_des3)
        for i in list_des3:
            file_in=i
            a=file_in.find('.')
            file_out=file_in[:-8-(len(file_in)-a-8)]+"_dec"+file_in[a:len(file_in)-8]
            TripleDES_decrypt(file_in,file_out,hash128)
            text_log.insert(INSERT,"File: "+file_in+"\nDecrypted\n")
            
#Label
dir_label=Label(window,text='Folder').place(x=0,y=0)
Key=Label(window,text="Key").place(x=0,y=50)
path=Label(window,text="Path").place(x=0,y=100)
#Entry

input_key_entry=Entry(window,bd=5,width=50)
input_key_entry.place(x=40,y=50)
input_key_button=Button(window, text="Generate Key",bg="Yellow",command=callback,width=20).place(x=400,y=50)
input_file_name=Entry(window,bd=5,width=50)
input_file_name.place(x=40,y=100)
input_filename=Button(window, text="Choose File",bg="Yellow",command=getFileName,width=20)
input_filename.place(x=400,y=100)
input_encrypt=Button(window, text="Encrypt",bg="Yellow",command=encrypt_,width=20)
input_decrypt=Button(window, text="Decrypt",bg="Yellow",command=decrypt_,width=20)

input_encrypt.place(x=100,y=150)
input_decrypt.place(x=100,y=200)
folder_encrypt=Button(window, text="Folder Encrypt",bg="Yellow",command=folder_encrypt,width=20)
folder_encrypt.place(x=280,y=150)
text_log.place(x=5,y=350)
folder_encrypt=Button(window, text="Folder Decrypt",bg="Yellow",command=folder_decrypt,width=20)
folder_encrypt.place(x=450,y=150)
input_dir_name=Entry(window,bd=5,width=50)
input_dir_name.place(x= 40,y=0)
input_dir_button=Button(window,text="Choose Folder",bg="Yellow",command=getDirname,width=20)
input_dir_button.place(x=400,y=0)
quit=Button(window,text="QUIT",command=window.quit,width=10).place(x=500,y=600)
def radio_algorithm(num):
    switcher= {
        1: "AES",
        2: "Triple DES",
    }
    return switcher.get(num,"Invalid algorithm")
def radio1_algorithm(num):
    switcher= {
        1: "SHA256",
        2: "MD5",
    }
    return switcher.get(num,"Invalid algorithm")
def select_algorithm():
    global algorithm
    algorithm=radio.get()
    text_log.insert(INSERT,"choosed "+radio_algorithm(radio.get())+"\n")
def select_hash():
    global hash_algorithm
    hash_algorithm=radio.get()
    text_log.insert(INSERT,"choosed "+radio1_algorithm(radio1.get())+"\n")
radio=IntVar()
radio_label=Label(window,text="Choose Encryption File Methods").place(x=5,y=250)
radio_button_1=Radiobutton(window,text="AES",variable=radio,value=1,command=select_algorithm)
radio_button_2=Radiobutton(window,text="Triple DES",variable=radio,value=2,command=select_algorithm)
radio_button_1.place(x=10,y=270)
radio_button_2.place(x=10,y=290)

radio1=IntVar()
radio1_label=Label(window,text="Choose Hash File Methods").place(x=300,y=250)
radio1_button_1=Radiobutton(window,text="SHA256",variable=radio1,value=1,command=select_hash)
radio1_button_2=Radiobutton(window,text="MD5",variable=radio1,value=2,command=select_hash)
radio1_button_1.place(x=300,y=270)
radio1_button_2.place(x=300,y=290)

# Delete Message
btn = Button(window, text='Delete Log', command=lambda: text_log.delete(1.0,END))
btn.place(x=10,y=600)
def hash_256():
    global filename 
    filein=open(filename,"rb")
    if(radio1!=2):  
        hash_digest = hash_SHA256(filein)
        text_log.insert(INSERT,"File" +filename+"\n SHA256: \n"+hash_digest+" \n" ) 
    else:
        hash_digest=hash_MD5(filein)
        text_log.insert(INSERT,"File" +filename+"\n MD5: \n"+hash_digest+" \n" ) 
    filein.close()

    
hash_SHA256_button=Button(window,text="Hash file", command=hash_256,bg="Yellow",width=20)
hash_SHA256_button.place(x=280,y=200)
